from floodsystem.flood import stations_level_over_threshold, stations_highest_rel_level
from floodsystem.stationdata import build_station_list, update_water_levels
import random


def test_stations_level_over_threshold():
    stations = build_station_list()
    tol = random.random()
    overtolerance = stations_level_over_threshold(stations, tol)
    value = [x[1] for x in overtolerance]
    for i in range(0, len(value)):
        assert type(value[i]) == float
        assert value[i] > tol


def test_stations_highest_rel_level():
    stations = build_station_list()
    update_water_levels(stations)
    N = random.randint(0,len(stations))
    output = stations_highest_rel_level(stations, N)
    assert len(output) == N
