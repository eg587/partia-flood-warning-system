from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_by_station_number


def run():
    """Requirements for Task 1E"""

    # Build list of stations
    stations = build_station_list()

    # Run functions for top nine stations
    N = 9
    # Print top rivers
    print(rivers_by_station_number(stations, N))
    


if __name__ == "__main__":
    print("*** Task 1E: CUED Part IA Flood Warning System ***")
    print("The rivers with the most monitoring stations are:")
    run()