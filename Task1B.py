from floodsystem.geo import stations_by_distance
from floodsystem.stationdata import build_station_list

def run():
    """produces a list of tuples for the 10 closet/
    futhest stations from Cambridge city centre"""

    # import the list of stations
    stations = build_station_list() 

    Cam_city_centre = (52.2053, 0.1218)

    # form a sorted list
    stations_list = stations_by_distance(stations, Cam_city_centre)

    # add name of town into list
    add_town = []
    for i in stations_list:
        for j in stations:
            if i[0] == j.name:
                add_town.append((i[0], j.town, i[1]))

    # print
    closest = add_town[:10]
    furthest = add_town[-10:]
    print("The 10 closest stations from Cambridge city centre are:")
    print(closest)
    print("The 10 furthest stations from Cambridge city centre are:")
    print(furthest)

if __name__ == "__main__":
    print("*** Task 1B: CUED Part IA Flood Warning System ***")
    run()