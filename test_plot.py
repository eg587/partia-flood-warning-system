from floodsystem.plot import plot_water_levels
from mock import patch
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_highest_rel_level
import datetime
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.plot import plot_water_level_with_fit


@patch("matplotlib.pyplot.show")
def test_plot_water_levels(self):
    #run function without showing graphs
    stations = build_station_list()
    update_water_levels(stations)
    top5levels = stations_highest_rel_level(stations, 5)
    top5names = [x[0] for x in top5levels]
    top5 = []
    for station in stations:
        if station.name in top5names:
            top5.append(station)
    for station in top5:
        dt = 10
        dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))
        plot_water_levels(station, dates, levels)
    assert type(top5) == list
    assert len(top5) == 5
    assert len(dates) == len(levels)

@patch("matplotlib.pyplot.show")
def test_plot_water_levels(self):
    stations = build_station_list()

    # Update latest level data for all stations
    update_water_levels(stations)

    # produce a list of 5 most at risk stations
    list_of_stations = stations_highest_rel_level(stations, 5)

    dt = 2
    p = 4

    for station in list_of_stations:
        for station0 in stations:
            if station0.name == station[0]:
                dates, levels = fetch_measure_levels(station0.measure_id, dt=datetime.timedelta(days=dt))
                plot_water_level_with_fit(station0, dates, levels, p)
                break
    assert len(levels) == len(dates)
    assert type(p) == int
    assert type(dt) == int

