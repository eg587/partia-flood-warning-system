from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_highest_rel_level
from floodsystem.plot import plot_water_levels
from floodsystem.datafetcher import fetch_measure_levels
import datetime


def run():
    # Build list of stations
    stations = build_station_list()
    update_water_levels(stations)
    top5levels = stations_highest_rel_level(stations, 5)
    top5names = [x[0] for x in top5levels]
    top5 = []
    for station in stations:
        if station.name in top5names:
            top5.append(station)
    for station in top5:
        dt = 10
        dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))
        plot_water_levels(station, dates, levels)


if __name__ == "__main__":
    print("*** Task 2E: CUED Part IA Flood Warning System ***")
    run()
