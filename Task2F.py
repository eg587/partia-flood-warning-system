from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_highest_rel_level
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.plot import plot_water_level_with_fit
import datetime

def run():
    """ plots the level data and the best-fit polynomial of degree 4 against time 
    for each of the 5 stations at which the current relative water level is 
    greatest and for a time period extending back 2 days"""

    # import the list of stations
    stations = build_station_list()

    # Update latest level data for all stations
    update_water_levels(stations)

    # produce a list of 5 most at risk stations
    list_of_stations = stations_highest_rel_level(stations, 5)

    dt = 2
    p = 4
    
    for station in list_of_stations:
        for station0 in stations:
            if station0.name == station[0]:
                dates, levels = fetch_measure_levels(station0.measure_id, dt=datetime.timedelta(days=dt))
                plot_water_level_with_fit(station0, dates, levels, p)
                break


if __name__ == "__main__":
    print("*** Task 2F: CUED Part IA Flood Warning System ***")
    run()
