from floodsystem.geo import rivers_with_station, stations_by_river
from floodsystem.stationdata import build_station_list

def run():
    """prints how many rivers have at least one monitoring station 
    and prints the 10 of these rivers in alphabetical order

    prints the stations located on the river aire, river cam, river
    thames in alphabetical order"""

    # Build list of stations
    stations = build_station_list()

    # Retrieve rivers that have at least one monitoring station
    station_rivers = rivers_with_station(stations)
    print()
    # Number of rivers retrieved
    print(len(station_rivers))
    print()
    # Alphabetically first 10 rivers retrieved
    print(sorted(station_rivers)[:10])
    print()

    # Retrieve dictionary mapping rivers to stations on that river
    riverToStations = stations_by_river(stations)
    print("Stations located on the River Aire: ", sorted(riverToStations["River Aire"]))
    print()
    print("Stations located on the River Cam: ", sorted(riverToStations["River Cam"]))
    print()
    print("Stations located on the River Thames: ", sorted(riverToStations["River Thames"]))

if __name__ == "__main__":
    print("*** Task 1D: CUED Part IA Flood Warning System ***")
    run()