from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.analysis import rising
import datetime


def run():
    """ plots the level data and the best-fit polynomial of degree 4 against time 
    for each of the 5 stations at which the current relative water level is 
    greatest and for a time period extending back 2 days"""

    # import the list of stations
    stations = build_station_list()

    # Update latest level data for all stations
    update_water_levels(stations)

    list_of_stations = []
    for station in stations:
        if station.relative_water_level() != None:
            list_of_stations.append(station)

    severe = []
    high = []
    moderate = []
    low = []
    dt=1
    for station in list_of_stations:
        if not station.town in severe and not station.town in high and not station.town in moderate and not station.town in low:
            if station.relative_water_level() > 2:
                severe.append(station.town)
            elif station.relative_water_level() > 1:
                dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))
                if len(dates) > 0:
                    if float(rising(dates, levels, 1)) > 0:
                        high.append(station.town)
                    else:
                        moderate.append(station.town)
            else:
                low.append(station.town)
    print("Towns with severe risk of flooding: ", severe)
    print()
    print("Towns with high risk of flooding: ", high)
    print()
    print("Towns with moderate risk of flooding: ", moderate)
    print()
    print("Towns with low risk of flooding: ", low)

if __name__ == "__main__":
    print("*** Task 2G: CUED Part IA Flood Warning System ***")
    run()