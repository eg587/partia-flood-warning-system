import dateutil.parser
from floodsystem.analysis import polyfit, rising
import matplotlib.dates

def test_polyfit():

    dates = [dateutil.parser.parse("2017-01-08 04:00:00+00:00"),
    dateutil.parser.parse("2017-01-08 03:45:00+00:00"),
    dateutil.parser.parse("2017-01-08 03:30:00+00:00"),
    dateutil.parser.parse("2017-01-08 03:15:00+00:00"),
    dateutil.parser.parse("2017-01-08 03:00:00+00:00"),
    dateutil.parser.parse("2017-01-08 02:45:00+00:00"),
    dateutil.parser.parse("2017-01-08 02:30:00+00:00"),
    dateutil.parser.parse("2017-01-08 02:15:00+00:00"),
    dateutil.parser.parse("2017-01-08 02:00:00+00:00"),
    dateutil.parser.parse("2017-01-08 01:45:00+00:00")]

    levels = [0.1, 0.5, 2, 0.3, 0.4, 0.3, 0.4, 3, 2.5, 2.0]

    assert(len(polyfit(dates, levels, 4)[0])>0), "check the function returns something for the polynomial"
    assert polyfit(dates, levels, 4)[1] == matplotlib.dates.date2num(dates)[0], "check the d0 is equal to the first date value"

def test_rising():

    dates = [dateutil.parser.parse("2017-01-08 04:00:00+00:00"),
    dateutil.parser.parse("2017-01-08 03:45:00+00:00"),
    dateutil.parser.parse("2017-01-08 03:30:00+00:00"),
    dateutil.parser.parse("2017-01-08 03:15:00+00:00"),
    dateutil.parser.parse("2017-01-08 03:00:00+00:00"),
    dateutil.parser.parse("2017-01-08 02:45:00+00:00"),
    dateutil.parser.parse("2017-01-08 02:30:00+00:00"),
    dateutil.parser.parse("2017-01-08 02:15:00+00:00"),
    dateutil.parser.parse("2017-01-08 02:00:00+00:00"),
    dateutil.parser.parse("2017-01-08 01:45:00+00:00")]

    levels = [0.1, 0.5, 2, 0.3, 0.4, 0.3, 0.4, 3, 2.5, 2.0]

    assert isinstance(rising(dates, levels, 1), float), "check the item returned is a number"