from floodsystem.geo import haversine
from floodsystem.geo import stations_by_distance
from floodsystem.geo import stations_within_radius
from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river
import random
from floodsystem.geo import rivers_by_station_number
from floodsystem.stationdata import build_station_list

#Task 1B tests
"""Test if the result is a float given random input"""
def test_haversine():
    p1=random.uniform(0,90)
    p2=random.uniform(0,90)
    p=(p1,p2)
    stations = build_station_list()
    for station in stations:
        distance = haversine(station.coord, p)
    assert type(distance)==float

def test_stations_by_distance():
    p1=random.uniform(0,90)
    p2=random.uniform(0,90)
    p=(p1,p2)
    stations = build_station_list()
    sorted_list = stations_by_distance(stations,p)
    for a in sorted_list:
        assert type(a[1])==float

#Task 1C test  
"""Test to see if distances are within radius"""
def test_stations_within_radius():
    stations = build_station_list()
    p1=random.uniform(0,90)
    p2=random.uniform(0,90)
    p=(p1,p2)
    r=random.uniform(0,1500)
    stat = stations_within_radius(stations,p,r)
    for station in stat:
        a=haversine(station.coord, p)
        assert a<=r

#Task 1D tests
"""Test for unique members in the list of rivers"""
def test_rivers_with_station():
    a=0
    stations = build_station_list()
    rivers = rivers_with_station(stations)
    for river in rivers:
        for river1 in rivers:
            if river != river1:
                a = 1
            else: 
                break
    assert a == 1 

def test_stations_by_river():
    stations = build_station_list()
    rivers_to_stations = stations_by_river(stations)
    assert type(rivers_to_stations)== dict

def test_Task1E():
    stations = build_station_list()
    top9 = rivers_by_station_number(stations, 9)
    assert len(top9) >= 9
    assert type(top9) == list
    assert type(top9[1]) == tuple