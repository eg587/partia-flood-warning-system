from floodsystem.geo import stations_within_radius
from floodsystem.stationdata import build_station_list

def run():
    """produce a list of stations within 10km of the Cambridge
    city centre
    """
    # import the list of stations
    stations = build_station_list()

    # define the coordinate of Cam city centre
    Cam_city_centre = (52.2053, 0.1218)

    # form a list of stations within 10km
    stations_within_10km = stations_within_radius(stations, Cam_city_centre, 10)

    # sort
    stations_within_10km.sort()

    # print
    print("Stations within 10km of the Cambridge city centre are:")
    print(stations_within_10km)

if __name__ == "__main__":
    print("*** Task 1C: CUED Part IA Flood Warning System ***")
    run()