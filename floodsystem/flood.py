def stations_level_over_threshold(stations, tol):
    overtolerance = []
    for station in stations:
        if station.relative_water_level() == None:
            continue
        elif station.relative_water_level() > tol:
            entry = (station.name, station.relative_water_level())
            overtolerance.append(entry)
    return sorted(overtolerance, key=lambda x: x[1], reverse=True)


def stations_highest_rel_level(stations, N):
    stationlist = stations_level_over_threshold(stations, 0)
    return stationlist[:N]
    