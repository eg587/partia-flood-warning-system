# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""

from .utils import sorted_by_key  # noqa

#haversine function
from math import radians, sin, asin, cos, sqrt
def haversine(p1,p2):
    la1, lo1 = p1
    la2, lo2 = p2
    la1 = radians(la1)
    lo1 = radians(lo1)
    la2 = radians(la2)
    lo2 = radians(lo2)

    #haversine formula
    d = sin((la2-la1)*0.5)**2 + cos(la1)*cos(la2)*sin((lo2-lo1)*0.5)**2
    R = 6371.0088 #Earth's radius in km
    return 2*R*asin(sqrt(d))

#Task 1B function
def stations_by_distance(stations, p):
        list_stations=[]
        for station in stations:
            distance = haversine(station.coord, p)
            list_stations.append((station.name,distance))
        sorted = sorted_by_key(list_stations,1)
        return sorted

#Task 1C function
def stations_within_radius(stations, centre, r):
    """This function returns a list of stations within r of
    a geographic coordinate x
    """
    list_stations = []

    for station in stations:
        distance = haversine(station.coord, centre)
        if distance <= r:
            list_stations.append(station.name)
    return list_stations

#Task 1D functions
def rivers_with_station(stations):
    """This function returns a list of rivers that have at 
    least one monitoring station"""

    rivers_station = []

    for station in stations:
        if not station.river in rivers_station:
             rivers_station.append(station.river)
    return rivers_station


def stations_by_river(stations):
    """function returns a dict that maps river names to a
    list of station objects on a given river
    """

    riversWithStation = rivers_with_station(stations)
    riverToStations = {riversWithStation[i]: [] for i in range(len(riversWithStation))}

    for station in stations:
        riverToStations[station.river].append(station.name)
    return riverToStations

#Task 1E
def rivers_by_station_number(stations, N):
    riverstations = []
    for station in stations:
        river = station.river  
        riverlist = [x[0] for x in riverstations] 
        countlist = [x[1] for x in riverstations]
        if river in riverlist:
            position = riverlist.index(river)
            stationcount = countlist[position]
            stationcount+=1
            entry = riverstations[position]
            riverstations.remove(entry)
            entry =  (river,stationcount) 
            riverstations.append(entry)
        else:
            entry = (river,1)
            riverstations.append(entry)

    riverstations.sort(key=lambda tup: tup[1],reverse=True)
    topN = riverstations[:N]

    i = N-1
    while topN[i][1]==riverstations[i+1][1]:
        topN.append(riverstations[i+1])
        i+=1
   
    return topN