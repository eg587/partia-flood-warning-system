import matplotlib
import numpy as np
from floodsystem.plot import plot_water_level_with_fit


def polyfit(dates, levels, p):
    """function that given the water level time history (dates, levels) 
    for a station computes a least-squares fit of a polynomial of degree 
    p to water level data"""
    x = matplotlib.dates.date2num(dates)
    # Using shifted x values, find coefficient of best-fit
    # polynomial f(x) of degree p
    p_coeff = np.polyfit(x - x[0], levels, p)

    # Convert coefficient into a polynomial that can be evaluated
    return (np.poly1d(p_coeff), x[0])

def rising(dates, levels, p):
    y = np.polyder(polyfit(dates, levels, p)[0], 1).coef[0]
    return y